package main

type ZabbixTemplate struct {
	ZabbixExport ZabbixExport `json:"zabbix_export" yaml:"zabbix_export" xml:"zabbix_export"`
	Id           int
}

type ZabbixExport struct {
	Version string `json:"version" yaml:"version" xml:"version"`
	/*Groups  struct {
		Group []struct {
			Name string `json:"name" yaml:"name" xml:"name"`
		} `json:"group" yaml:"group" xml:"group"`
	} `json:"groups" yaml:"groups" xml:"groups"`*/
	Templates struct {
		Template Template `json:"template" yaml:"template" xml:"template"`
	} `json:"templates" yaml:"templates" xml:"templates"`
	Triggers struct {
		Trigger Trigger `json:"trigger" yaml:"trigger" xml:"trigger"`
	} `json:"triggers" yaml:"triggers" xml:"triggers"`
}

// Break out specific struct into more well defined structs
type Template struct {
	Template    string `json:"template" yaml:"template" xml:"template"`
	Name        string `json:"name" yaml:"name" xml:"name"`
	Description string `json:"description" yaml:"description" xml:"description"`
	Groups      struct {
		Group []struct {
			Name string `json:"name" yaml:"name" xml:"name"`
		} `json:"group" yaml:"group" xml:"group"`
	} `json:"groups" yaml:"groups" xml:"groups"`
	Applications struct {
		Application []struct {
			Name string `json:"name" yaml:"name" xml:"name"`
		} `json:"application" yaml:"application" xml:"application"`
	} `json:"applications" yaml:"applications" xml:"applications"`
	Items struct {
		Item Item `json:"item" yaml:"item" xml:"item"`
	} `json:"items" yaml:"items" xml:"items"`
	DiscoveryRules string `json:"discovery_rules" yaml:"discovery_rules" xml:"discovery_rules"`
	Macros         string `json:"macros" yaml:"macros" xml:"macros"`
	Templates      string `json:"templates" yaml:"templates" xml:"templates"`
	Screens        string `json:"screens" yaml:"screens" xml:"screens"`
}
type Item []struct {
	AllowedHosts string `json:"allowed_hosts" yaml:"allowed_hosts" xml:"allowed_hosts"`
	Applications struct {
		Application []struct {
			Name string `json:"name" yaml:"name" xml:"name"`
		} `json:"application" yaml:"application" xml:"application"`
	} `json:"applications" yaml:"applications" xml:"applications"`
	Authtype             string `json:"authtype" yaml:"authtype" xml:"authtype"`
	DataType             string `json:"data_type" yaml:"data_type" xml:"data_type"`
	Delay                string `json:"delay" yaml:"delay" xml:"delay"`
	DelayFlex            string `json:"delay_flex" yaml:"delay_flex" xml:"delay_flex"`
	Delta                string `json:"delta" yaml:"delta" xml:"delta"`
	Description          string `json:"description" yaml:"description" xml:"description"`
	Formula              string `json:"formula" yaml:"formula" xml:"formula"`
	History              string `json:"history" yaml:"history" xml:"history"`
	InventoryLink        string `json:"inventory_link" yaml:"inventory_link" xml:"inventory_link"`
	IpmiSensor           string `json:"ipmi_sensor" yaml:"ipmi_sensor" xml:"ipmi_sensor"`
	Key                  string `json:"key" yaml:"key" xml:"key"`
	Logtimefmt           string `json:"logtimefmt" yaml:"logtimefmt" xml:"logtimefmt"`
	Multiplier           string `json:"multiplier" yaml:"multiplier" xml:"multiplier"`
	Name                 string `json:"name" yaml:"name" xml:"name"`
	Params               string `json:"params" yaml:"params" xml:"params"`
	Password             string `json:"password" yaml:"password" xml:"password"`
	Port                 string `json:"port" yaml:"port" xml:"port"`
	Privatekey           string `json:"privatekey" yaml:"privatekey" xml:"privatekey"`
	Publickey            string `json:"publickey" yaml:"publickey" xml:"publickey"`
	SnmpCommunity        string `json:"snmp_community" yaml:"snmp_community" xml:"snmp_community"`
	SnmpOid              string `json:"snmp_oid" yaml:"snmp_oid" xml:"snmp_oid"`
	Snmpv3Authpassphrase string `json:"snmpv3_authpassphrase" yaml:"snmpv3_authpassphrase" xml:"snmpv3_authpassphrase"`
	Snmpv3Authprotocol   string `json:"snmpv3_authprotocol" yaml:"snmpv3_authprotocol" xml:"snmpv3_authprotocol"`
	Snmpv3Contextname    string `json:"snmpv3_contextname" yaml:"snmpv3_contextname" xml:"snmpv3_contextname"`
	Snmpv3Privpassphrase string `json:"snmpv3_privpassphrase" yaml:"snmpv3_privpassphrase" xml:"snmpv3_privpassphrase"`
	Snmpv3Privprotocol   string `json:"snmpv3_privprotocol" yaml:"snmpv3_privprotocol" xml:"snmpv3_privprotocol"`
	Snmpv3Securitylevel  string `json:"snmpv3_securitylevel" yaml:"snmpv3_securitylevel" xml:"snmpv3_securitylevel"`
	Snmpv3Securityname   string `json:"snmpv3_securityname" yaml:"snmpv3_securityname" xml:"snmpv3_securityname"`
	Status               string `json:"status" yaml:"status" xml:"status"`
	Trends               string `json:"trends" yaml:"trends" xml:"trends"`
	Type                 string `json:"type" yaml:"type" xml:"type"`
	Units                string `json:"units" yaml:"units" xml:"units"`
	Username             string `json:"username" yaml:"username" xml:"username"`
	ValueType            string `json:"value_type" yaml:"value_type" xml:"value_type"`
	// The below seems to be causing marshal issues
	/*
		Valuemap struct {
			Name string `json:"name" yaml:"name" xml:"name"`
		} `json:"valuemap" yaml:"valuemap" xml:"valuemap"`
	*/
}

type Trigger []struct {
	Expression   string `json:"expression" yaml:"expression" xml:"expression"`
	Name         string `json:"name" yaml:"name" xml:"name"`
	URL          string `json:"url" yaml:"url" xml:"url"`
	Status       string `json:"status" yaml:"status" xml:"status"`
	Priority     string `json:"priority" yaml:"priority" xml:"priority"`
	Description  string `json:"description" yaml:"description" xml:"description"`
	Type         string `json:"type" yaml:"type" xml:"type"`
	Dependencies string `json:"dependencies" yaml:"dependencies" xml:"dependencies"`
}
