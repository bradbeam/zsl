package main

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Server   string
	Port     string
	Path     string
	Username string
	Password string
}

func loadConfig(templateFile string, configFile string) (*ZabbixTemplate, *Config, error) {
	myTemplate := &ZabbixTemplate{}
	myZbxExport := &ZabbixExport{}
	myConfig := &Config{}

	cfg, err := ioutil.ReadFile(configFile)
	if err != nil {
		return myTemplate, myConfig, err
	}

	tmpl, err := ioutil.ReadFile(templateFile)
	if err != nil {
		return myTemplate, myConfig, err
	}

	err = json.Unmarshal(cfg, myConfig)
	if err != nil {
		return myTemplate, myConfig, err
	}

	log.Println("Attempting to decode as json")
	err = json.Unmarshal(tmpl, myTemplate)
	if err != nil {
		// attempt to umarshal as yaml
		log.Println("Attempting to decode as yaml")
		err = yaml.Unmarshal(tmpl, myTemplate)
		if err != nil {
			// attempt to umarshal as xml
			log.Println("Attempting to decode as xml")
			err = xml.Unmarshal(tmpl, myZbxExport)
			if err != nil {
				return myTemplate, myConfig, err
			}
			myTemplate.ZabbixExport = *myZbxExport
		}
	}

	return myTemplate, myConfig, nil
}
