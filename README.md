# zsl

## About

zsl is an attempt to make a json or yaml structure that will allow for better management of zabbix templates.

There is a severe limitation with regards to the zabbix api configuration import where a template must be a xml document escaped as a string in the payload. This is just plain awful.

With zsl, the interaction with the zabbix api happens at the `template.create`, `item.create`, `trigger.create`, etc levels. This allows us to define a zabbix template is a more reasonable format and get around using a poorly designed api endpoint.

## Usage


## Example

### json


### yaml


### xml?
