package main

import (
	"errors"
	"log"
	"strconv"

	"github.com/AlekSi/zabbix"
)

func createTemplate(template string, hgs []string, api zabbix.API) (int, error) {
	// Might need to denote this elsewhere,
	// but we'll say a templateid of 0 = non-existant
	templateid := 0
	var err error

	// Thx homie
	// http://www.sourcecode.net.br/2014/02/zabbix-api-with-golang.html
	res, err := api.Call("template.exists", zabbix.Params{"host": template, "output": "extend"})
	if err != nil {
		return templateid, err
	}

	if res.Result == false {
		// Make sure "Templates" hostgroup exists
		// We may want to discuss if this should be always added to
		// "Templates" or if it should only be added to the hostgroup
		// created previously from this program
		parameters := map[string][]string{"name": hgs}
		res, err := api.Call("hostgroup.get", zabbix.Params{"filter": parameters, "output": "extend"})
		if err != nil {
			return templateid, err
		}

		// TODO: Create hostgroups on the fly
		if len(res.Result.([]interface{})) == 0 {
			return templateid, errors.New("Not all hostgroups exist ")
		}

		groups := []map[string]string{}
		for _, id := range res.Result.([]interface{}) {
			groups = append(groups, map[string]string{"groupid": id.(map[string]interface{})["groupid"].(string)})
			//group = append(group, id.(map[string]interface{})["groupid"].(string))
		}

		log.Println("[createTemplate] Info: Creating template " + template)
		zp := zabbix.Params{"host": template, "groups": groups}
		res, err = api.Call("template.create", zp)
		if err != nil {
			return templateid, err
		}
	} else {
		log.Println("[createTemplate] Info: Template " + template + " exists")
	}

	// Look up template id
	parameters := map[string]string{"host": template}
	res, err = api.Call("template.get", zabbix.Params{"filter": parameters, "output": "shorten"})
	if err != nil {
		return templateid, err
	}

	templateid, err = strconv.Atoi(res.Result.([]interface{})[0].(map[string]interface{})["templateid"].(string))
	if err != nil {
		return templateid, err
	}

	return templateid, nil

}

func createItems(templateid int, items Item, api zabbix.API) error {
	// Do some nonsense with applications
	myapps := make(map[string]string)
	for _, item := range items {
		for _, application := range item.Applications.Application {
			app := application.Name
			// I guess this is the right way to do it, although
			// it probably wouldn't be bad if we just assigned
			// it every time
			if _, ok := myapps[app]; !ok {
				myapps[app] = ""
			}
		}
	}

	for app, _ := range myapps {
		log.Println("[createItems] Checking if " + app + " exists")
		res, err := api.Call("application.exists", zabbix.Params{"hostid": templateid, "name": app, "output": "short"})
		if err != nil {
			return err
		}
		if res.Result == false {
			// Don't need res from this, will do app id lookup later
			_, err = api.Call("application.create", zabbix.Params{"hostid": templateid, "name": app})
			if err != nil {
				return err
			}
		}

		res, err = api.Call("application.get", zabbix.Params{"hostids": templateid, "output": "short", "filter": map[string]string{"name": app}})
		if err != nil {
			return err
		}

		appid := res.Result.([]interface{})[0].(map[string]interface{})["applicationid"].(string)
		if err != nil {
			return err
		}

		// #money
		myapps[app] = appid
	}

	// Go through our items
	myitems := make([]zabbix.Params, 0)
	newitem := zabbix.Params{}

	for _, item := range items {
		newitem = zabbix.Params{
			"hostid":                templateid,
			"allowed_hosts":         item.AllowedHosts,
			"authtype":              item.Authtype,
			"data_type":             item.DataType,
			"delay":                 item.Delay,
			"delay_flex":            item.DelayFlex,
			"delta":                 item.Delta,
			"description":           item.Description,
			"formula":               item.Formula,
			"history":               item.History,
			"inventory_link":        item.InventoryLink,
			"ipmi_sensor":           item.IpmiSensor,
			"key_":                  item.Key,
			"logtimefmt":            item.Logtimefmt,
			"multiplier":            item.Multiplier,
			"name":                  item.Name,
			"params":                item.Params,
			"password":              item.Password,
			"port":                  item.Port,
			"privatekey":            item.Privatekey,
			"publickey":             item.Publickey,
			"snmp_community":        item.SnmpCommunity,
			"snmp_oid":              item.SnmpOid,
			"snmpv3_authpassphrase": item.Snmpv3Authpassphrase,
			"snmpv3_authprotocol":   item.Snmpv3Authprotocol,
			"snmpv3_contextname":    item.Snmpv3Contextname,
			"snmpv3_privpassphrase": item.Snmpv3Privpassphrase,
			"snmpv3_privprotocol":   item.Snmpv3Privprotocol,
			"snmpv3_securitylevel":  item.Snmpv3Securitylevel,
			"snmpv3_securityname":   item.Snmpv3Securityname,
			"status":                item.Status,
			"trends":                item.Trends,
			"type":                  item.Type,
			"units":                 item.Units,
			"username":              item.Username,
			"value_type":            item.ValueType,
			//"valuemap": item.Valuemap,
		}

		applications := []string{}
		for _, application := range item.Applications.Application {
			applications = append(applications, myapps[application.Name])
		}
		newitem["applications"] = applications

		myitems = append(myitems, newitem)
	}

	for _, item := range myitems {
		res, err := api.Call("item.get", zabbix.Params{"hostids": item["hostid"].(int), "output": "short", "filter": map[string]string{"key_": item["key_"].(string)}})

		if err != nil {
			// If we cant check if an item exists, we most likely have an issue with the
			// zabbix server. We'll just bail for now
			log.Println("[checkIfItemExists] Error: " + err.Error())
			return nil
		}

		if len(res.Result.([]interface{})) == 0 {
			log.Println("[createItems] Info: Creating " + item["name"].(string))

			res, err = api.Call("item.create", item)
			if res.Error != nil {
				return res.Error
			}
			if err != nil {
				return err
			}
		} else {
			log.Println("[createItems] Info: Item " + item["name"].(string) + " already exists")
		}
	}

	return nil
}

func createTriggers(templateid int, triggers Trigger, api zabbix.API) error {
	// Get a list of all triggers on a host ( hopefully )
	res, err := api.Call("trigger.get", zabbix.Params{"hostids": templateid})
	if err != nil {
		return err
	}

	// Might need to check on length of result
	//log.Println(res)
	existingTriggers := map[string]string{}
	if len(res.Result.([]interface{})) > 0 {
		for _, val := range res.Result.([]interface{}) {
			existingTriggers[val.(map[string]interface{})["description"].(string)] = ""
		}
	}

	for _, trigger := range triggers {
		if _, ok := existingTriggers[trigger.Name]; !ok {
			log.Println("[createTriggers] Info: Creating trigger " + trigger.Name)

			// This seems really awkward that you cant assign a trigger
			// to a hostid ( host or template )
			// I guess it assigns it to whatever is referred to in the
			// expression
			// Oh zabbix, go fly a kite. desc = name, comment = desc
			triggerDef := zabbix.Params{
				"description": trigger.Name,
				"expression":  trigger.Expression,
				"status":      trigger.Status,
				"priority":    trigger.Priority,
				"type":        trigger.Type,
				"url":         trigger.URL,
				"comments":    trigger.Description,
				//"dependencies": trigger.Dependencies,

			}

			_, err = api.Call("trigger.create", triggerDef)
			if err != nil {
				return err
			}
		} else {
			log.Println("[createTriggers] Info: Trigger " + trigger.Name + " already exists")
		}

		//log.Println(res)
	}

	return nil
}
