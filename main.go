package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/AlekSi/zabbix"
)

var templateFile = flag.String("t", "", "The path to the json file representing the template")
var configFile = flag.String("c", "", "The path to the json file with the zabbix configuration information")

func main() {
	flag.Parse()

	if *templateFile == "" {
		log.Println("Template File not specified")
		flag.PrintDefaults()
	}

	if *configFile == "" {
		log.Println("Config File not specified")
		flag.PrintDefaults()
	}

	template, config, err := loadConfig(*templateFile, *configFile)
	if err != nil {
		log.Fatal(err.Error())
	}

	server := config.Server
	if config.Port != "" {
		server += ":" + config.Port
	}
	server += config.Path

	api := zabbix.NewAPI(server + "/api_jsonrpc.php")

	// We'll tweak the API to allow a timeout on connection
	hclient := &http.Client{Timeout: time.Duration(15 * time.Second)}
	api.SetClient(hclient)

	_, err = api.Login(config.Username, config.Password)
	if err != nil {
		log.Println(err.Error())
		log.Fatal("Error: Failed to connect to zabbix")
	}

	// Load up hostgroups for a template
	hgs := []string{}
	for _, group := range template.ZabbixExport.Templates.Template.Groups.Group {
		hgs = append(hgs, group.Name)
	}

	// Create the template
	templateid, err := createTemplate(template.ZabbixExport.Templates.Template.Name, hgs, *api)
	if templateid == 0 || err != nil {
		log.Fatal("Failed to create template: " + err.Error())
	}
	template.Id = templateid

	err = createItems(template.Id, template.ZabbixExport.Templates.Template.Items.Item, *api)
	if err != nil {
		log.Fatal("Failed to create items: " + err.Error())
	}

	err = createTriggers(template.Id, template.ZabbixExport.Triggers.Trigger, *api)
	if err != nil {
		log.Fatal("Failed to create triggers: " + err.Error())
	}
}
